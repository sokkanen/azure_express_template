const express = require('express')

const app = express()

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.get('/', (_req, res) => {
    res.status(200).send('Hello cloud world!')
})

const port = process.env.PORT || 8080
const environment = process.env.NODE_ENV 

if (environment !== 'test') {
    app.listen(port, () => {
        console.log(`Application is listening to port ${port}`)
    })
}

module.exports = app